<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;

class AdminMediasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $photos = Photo::all();
        
        return view('admin.media.index', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.media.create');
    }
    
    public function store(Request $request)
    {
        $file = $request->file('file');
        
        $name = time().$file->getClientOriginalName();
        $file->move('public/images', $name);
        
        Photo::create(['file' => $name]);
    }
    
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        
        unlink(base_path().'/'.$photo->file);
        
        $photo->delete();
        
        return redirect('/admin/medias');
    }
}
